import os
import json
import argparse
import boto3
from flask import Flask

app = Flask(__name__)

s3_client = boto3.client('s3', 
                aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
                endpoint_url='https://s3.cern.ch')


def read_records_from_s3(bucket_name, object_name):
    download_filepath = 'downloaded.json'
    s3_client.download_file(bucket_name, object_name, download_filepath)
    
    with open(download_filepath, 'r') as f:
        records = json.loads(f.read())
        
    os.remove(download_filepath)
        
    return records


@app.route("/gpu-usage")
def gpu_usage():
    records = read_records_from_s3(app.config.get('bucket_name'), app.config.get('object_name'))

    res = []

    for node, value_dict in records.items():
        value_dict['node'] = node
        res.append(value_dict)

    return json.dumps(res)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--instance', type=str, help='prod or staging')
    parser.add_argument('--bucket', type=str, help='bucket name')

    args = parser.parse_args()
    app.config['bucket_name'] = args.bucket
    app.config['object_name'] = args.instance + '_gpus_healthcheck.json'

    app.run(host='0.0.0.0', port=4495)
