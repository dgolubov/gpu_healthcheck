import os
import time
import argparse
import json
import boto3
from prometheus_client.core import GaugeMetricFamily, REGISTRY, CounterMetricFamily
from prometheus_client import start_http_server

s3_client = boto3.client('s3', 
                aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
                endpoint_url='https://s3.cern.ch')

status_mapping = {
    'available': 1,
    'busy': 1,
    'unavailable': 0,
}

parser = argparse.ArgumentParser()
parser.add_argument('--instance', type=str, help='prod or staging')
parser.add_argument('--bucket', type=str, help='bucket name')
args = parser.parse_args()

bucket_name = args.bucket
object_name = args.instance + '_gpus_healthcheck.json'


def read_records_from_s3(bucket_name, object_name):
    download_filepath = 'downloaded.json'
    s3_client.download_file(bucket_name, object_name, download_filepath)
    
    with open(download_filepath, 'r') as f:
        records = json.loads(f.read())
        
    os.remove(download_filepath)
        
    return records


class CustomCollector(object):
    def __init__(self):
        pass

    def collect(self):
        records = read_records_from_s3(bucket_name, object_name)

        if not records:
            return

        labels = list(records[list(records.keys())[0]].keys())

        g = GaugeMetricFamily("GPUAccess", '1 - available or busy, 0 - unavailable', labels=labels)

        for node, value_dict in records.items():
            #g.add_metric([node], status_mapping[value_dict['status']])
            metrics_label_values = [value_dict[label] if value_dict[label] is not None else '' for label in labels]
            g.add_metric(metrics_label_values, status_mapping[value_dict['status']])
            yield g


if __name__ == '__main__':
    start_http_server(80)
    REGISTRY.register(CustomCollector())
    while True:
        time.sleep(1)
