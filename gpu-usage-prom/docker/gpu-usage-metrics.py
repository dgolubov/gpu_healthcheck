import time
from kubernetes import client, config
from prometheus_client.core import GaugeMetricFamily, REGISTRY, CounterMetricFamily
from prometheus_client import start_http_server


config.load_incluster_config()
v1 = client.CoreV1Api()


class CustomCollector(object):
    def __init__(self):
        pass

    def collect(self):
        response_nodes = v1.list_node(label_selector='node-role.kubernetes.io/gpu=true')
        gpu_nodes = [node for node in response_nodes.items]

        for node in gpu_nodes:
            pods = v1.list_pod_for_all_namespaces(field_selector = 'spec.nodeName=' + node.metadata.name)

            usage_pods = []
            for pod in pods.items:
                for container in pod.spec.containers:
                    if container.resources and container.resources.requests and 'nvidia.com/gpu' in container.resources.requests and container.resources.requests['nvidia.com/gpu'] in ['1']:
                        usage_pods.append(pod)

            labels = ['node', 'pod', 'namespace', 'status', 'creation_timestamp']
            g = GaugeMetricFamily("GPUUsage", '1 - in usage, 0 - available', labels=labels)

            if not usage_pods:
                print('no pods using node:', node.metadata.name)
                g.add_metric([node.metadata.name, '', '', '', ''], 0)
                print('metrics added')
                yield g
                print('metrics yielded')
                continue

            for usage_pod in usage_pods:
                metrics_label_values = [node.metadata.name,
                                        usage_pod.metadata.name,
                                        usage_pod.metadata.namespace,
                                        usage_pod.status.phase,
                                        str(usage_pod.metadata.creation_timestamp)]
                print(metrics_label_values)

                g.add_metric(metrics_label_values, 1)
                print('metrics added')
                yield g
                print('metrics yielded')


if __name__ == '__main__':
    start_http_server(80)
    REGISTRY.register(CustomCollector())
    while True:
        time.sleep(1)
