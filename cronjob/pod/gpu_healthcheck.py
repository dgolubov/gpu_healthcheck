import tensorflow as tf
import sys

visible_devices = tf.config.list_physical_devices('GPU')

if len(visible_devices) == 0:
    sys.exit('GPU not visible by tensorflow')
