import os
import sys
import time
import datetime
import yaml
import json
import argparse
from collections import defaultdict
from kubernetes import client, config
import boto3


def set_default_records(node_names):
    default_record = {
        'node': None,
        'last_test_time_start': None,
        'last_test_time_finish': None,
        'last_success_time_start': None,
        'last_success_time_finish': None,
        'last_fail_time_start': None,
        'last_fail_time_finish': None,
        'status': None,
    }
    
    return {node_name: default_record.copy() for node_name in node_names}


def records_exist_in_s3(bucket_name, object_name):
    session = boto3.Session( 
         aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'), 
         aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY')
    )

    s3 = session.resource('s3', endpoint_url='https://s3.cern.ch')

    bucket = s3.Bucket(bucket_name)

    for my_bucket_object in bucket.objects.all():
        if object_name == my_bucket_object.key:
            return True

    return False


def datetime_handler(x):
    if isinstance(x, datetime.datetime):
        return x.isoformat()
    
    raise TypeError("Unknown type")


def read_records_from_s3(s3_client, bucket_name, object_name):
    download_filepath = 'downloaded.json'
    s3_client.download_file(bucket_name, object_name, download_filepath)
    
    with open(download_filepath, 'r') as f:
        records = json.loads(f.read())
        
    os.remove(download_filepath)
        
    return records


def store_records_to_json(filepath, records):
    res = {}

    for node_name, value_dict in records.items():
        res[node_name] = value_dict

    with open(filepath, 'w') as f:
        json.dump(res, f, default=datetime_handler)


def push_records_to_s3(s3_client, bucket_name, object_name, filepath):
    s3_client.upload_file(filepath, bucket_name, object_name)


def record_completed_pod_state(node_name, pod_state, records):
    records[node_name]['last_test_time_finish'] = pod_state.terminated.finished_at
    
    if pod_state.terminated.exit_code == 0:
        records[node_name]['last_success_time_start'] = pod_state.terminated.started_at
        records[node_name]['last_success_time_finish'] = pod_state.terminated.finished_at
        records[node_name]['status'] = 'available'
    else:
        records[node_name]['last_fail_time_start'] = pod_state.terminated.started_at
        records[node_name]['last_fail_time_finish'] = pod_state.terminated.finished_at
        records[node_name]['status'] = 'unavailable'


def start_pods(k8s_api, node_names, pod_spec, namespace):
    pod_name_base = pod_spec['metadata']['name']
    
    for node_name in node_names:
        pod_spec['metadata']['name'] = pod_name_base + node_name
        pod_spec['spec']['nodeName'] = node_name

        pod = k8s_api.create_namespaced_pod(namespace=namespace, body=pod_spec)


def monitor_pods(k8s_api, pod_name_base, node_names, namespace, records, interval=5):
    total_time = 0

    completed = set()
    busy = set()

    while len(completed) < len(node_names) and total_time < 4 * interval:
        for node_name in node_names:
            pod_name = pod_name_base + node_name

            if pod_name in completed or pod_name in busy:
                continue

            pod = k8s_api.read_namespaced_pod_status(namespace=namespace, name=pod_name)
            records[node_name]['node'] = node_name
            records[node_name]['last_test_time_start'] = pod.status.start_time

            if pod.status.phase == 'Failed':
                records[node_name]['status'] = 'busy'
                busy.add(node_name)
            elif pod.status.container_statuses[0].state.terminated:
                record_completed_pod_state(node_name, pod.status.container_statuses[0].state, records)
                completed.add(node_name)
            else:
                records[node_name]['status'] = 'unavailable'

        total_time += interval
        time.sleep(interval)


def delete_pods(k8s_api, pod_name_base, node_names, namespace):
    for node_name in node_names:
        pod_name = pod_name_base + node_name
        k8s_api.delete_namespaced_pod(namespace=namespace, name=pod_name)


def print_records(records):
    for node_name, pod_info in records.items():
        print('\n', node_name)
        for metrics, value in pod_info.items():
            print('\t', metrics, ':', value)


def get_nodes(filename):
    nodes = []
    with open(filename, 'r') as f:
        for line in f.readlines():
            nodes.append(line.strip())

    return nodes


config.load_incluster_config()
v1 = client.CoreV1Api()

parser = argparse.ArgumentParser()
parser.add_argument('--instance', type=str, help='prod or staging')
parser.add_argument('--namespace', type=str, help='namespace')
parser.add_argument('--bucket', type=str, help='bucket name')

args = parser.parse_args()
namespace = args.namespace
bucket_name = args.bucket

nodes_filename = 'gpu-nodes-prod.txt' if args.instance == 'prod' \
            else 'gpu-nodes-staging.txt' 

node_names = get_nodes(nodes_filename)

with open('template-prod.yaml') as f:
    pod_spec = yaml.full_load(f)
    pod_name_base = pod_spec['metadata']['name']

s3_client = boto3.client('s3', 
                    aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                    aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
                    endpoint_url='https://s3.cern.ch')

object_name = args.instance + '_gpus_healthcheck.json'
records_filepath = 'gpus_healthcheck.json'

if records_exist_in_s3(bucket_name, object_name):
    records = read_records_from_s3(s3_client, bucket_name, object_name)
else:
    records = set_default_records(node_names)

try:
    start_pods(v1, node_names, pod_spec, namespace)
    monitor_pods(v1, pod_name_base, node_names, namespace, records)
    print_records(records)
    store_records_to_json(records_filepath, records)
    push_records_to_s3(s3_client, bucket_name, object_name, records_filepath)
finally:
    delete_pods(v1, pod_name_base, node_names, namespace)
