### Create S3 secret

Get secret data with:
(-n for newline)

``` echo -n KEY | base64 ```

```
kubectl -n NS apply -f secret.yaml
```

### Create a pod or cronjob to check GPUs
```
kubectl -n NS apply -f cronjob/deploy/pod.yaml
```
```
kubectl -n NS apply -f cronjob/deploy/cronjob.yaml
```

### Expose metrics via REST enpoint or as Prometheus metrics
```
kubectl -n NS apply -f backend/deploy/deployment.yaml
```
```
kubectl -n NS apply -f prometheus/deploy/deployment.yaml
```

